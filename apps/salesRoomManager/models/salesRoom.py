# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     salesRoom.py
# Description:  专卖店信息数据模型
# Author:       'zhouhanlin'
# CreateDate:   2020/02/20
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from sqlalchemy import Column, String, Integer

from apps.common.utils.base_model import BaseModel


class SalesRoom(BaseModel):
    """
    专卖店数据模型
    SQL建表语句：
                drop table if exists `t_sales_room`;
                create table `t_sales_room` (
                `id` int ( 11 ) not null auto_increment comment '主键',
                `key` varchar ( 512 ) not null unique comment 'country+distributor+room_id组合键，值唯一',
                `room_id` varchar ( 255 ) not null comment '门店id',
                `country` varchar ( 255 ) not null comment '国家',
                `distributor_id` varchar ( 255 ) not null comment '经销商id',
                `shopkeeper_name` varchar ( 255 ) not null comment '店主姓名',
                `certificate_id` varchar ( 255 ) not null comment '证件号',
                `contact_1` varchar ( 32 ) null comment '联系方式1',
                `contact_2` varchar ( 32 ) null comment '联系方式2',
                `contact_3` varchar ( 32 ) null comment '联系方式3',
                `create_time` datetime DEFAULT CURRENT_TIMESTAMP comment '专卖店创建时间',
                `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '专卖店信息修改时间',
                primary key ( `id` )
                ) default charset = utf8 comment '专卖店信息表';
    """
    __tablename__ = "t_sales_room"
    id = Column("id", Integer, primary_key=True, autoincrement=True, comment="主键")
    key = Column("key", String(500), nullable=False, unique=True, comment="country+distributor+room_id组合键，值唯一")
    room_id = Column("room_id", String(255), nullable=False, comment="门店id")
    country = Column("country", String(255), nullable=False, comment="国家")
    distributor_id = Column("distributor_id", String(255), nullable=False, comment="经销商id")
    shopkeeper_name = Column("shopkeeper_name", String(255), nullable=False, comment="店主姓名")
    certificate_id = Column("certificate_id", String(255), nullable=False, comment="证件号")
    contact_1 = Column("contact_1", String(32), nullable=True, comment="联系方式1")
    contact_2 = Column("contact_2", String(32), nullable=True, comment="联系方式2")
    contact_3 = Column("contact_3", String(32), nullable=True, comment="联系方式3")

    # 给表添加注释
    __table_args__ = ({'comment': '专卖店信息表'})
