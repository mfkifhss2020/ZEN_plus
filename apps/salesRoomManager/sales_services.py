# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN_plus
# FileName:     sales_services.py
# Description:  DOTO
# Author:       zhouhanlin
# CreateDate:   2021/02/07
# Copyright ©2011-2021. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import logging
import platform

from apps.common.http.respBody import MsgDesc
from apps.common.utils.log_service import auto_log
from apps.common.utils.base_service import BaseService
from apps.salesRoomManager.models.salesRoom import SalesRoom

# 在windows上调式运行，使用flask的wsgi模块运行，需要抓取werkzeug日志
# 在linux上运行，通常会使用gunicorn替代wsgi，则需要抓取gunicorn.error日志
logger = logging.getLogger('werkzeug') if "Windows" in platform.system() else logging.getLogger('gunicorn.error')


class SalesBaseService(BaseService):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    @auto_log
    def get_shop_no(cls) -> tuple:
        sales_room_object_list = SalesRoom.query.all()
        shop_no_list = [x.room_id for x in sales_room_object_list]
        shop_no_list = list(set(shop_no_list))
        return True, dict(code=200, message=MsgDesc.h_200_200.value, data=shop_no_list)
