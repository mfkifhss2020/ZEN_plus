# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN_plus
# FileName:     sales_views.py
# Description:  DOTO
# Author:       zhouhanlin
# CreateDate:   2021/02/07
# Copyright ©2011-2021. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from flask_restful import reqparse

from apps.common.utils.base_resource import BaseResource
from apps.common.http.response_formatter import RespFormatter
from apps.salesRoomManager.sales_services import SalesBaseService
from apps.common.http.decorators import login_required, access_auth


class SalesBaseView(BaseResource):
    def __init__(self):
        super().__init__()
        self.sales_base_obj = SalesBaseService()


class ShopNoListView(SalesBaseView):

    @login_required
    @access_auth
    def get(self):
        """
        获取shop.No列表
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        kwargs = parse.parse_args(strict=True)
        kwargs.clear()
        status, result = SalesBaseService.get_shop_no()
        if status:
            self.logger.info(self.logger_formatter + " 成功...")
            result["result"] = "success"
        else:
            self.logger.error(self.logger_formatter + " 失败...")
        return RespFormatter.body(**result), int(str(result.get("code"))[0:3])
