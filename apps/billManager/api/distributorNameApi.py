# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     distributorNameApi.py
# Description:  查询经销商名字Api
# Author:       'zhouhanlin'
# CreateDate:   2020/03/17
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from flask_restful import reqparse, fields, marshal

from apps.common.utils.base_resource import BaseResource
from apps.common.http.respBody import RespBody, MsgDesc
from apps.userManager.utils.userSupplement import base_user_info
from apps.common.http.decorators import login_required, access_auth

resource_fields = {
    "distributor_id": fields.String,
    "distributor_name": fields.String
}


class DistributorName(BaseResource):

    # 装饰器判断用户是否已经登录
    @login_required
    @access_auth
    def get(self):
        """
        查询专卖店经销商，普通经销商的名字，支持模糊匹配
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取args中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("keyword", type=str, required=True, location='args')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 获得查询关键字
        keyword = args.get("keyword")
        result = base_user_info(keyword)
        if result['flag']:
            user_info_lists = result.get('data')
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success",
                                   code=200,
                                   string=MsgDesc.h_200_200.value,
                                   data=marshal(user_info_lists, resource_fields)), 200
        else:
            self.logger.error(MsgDesc.h_404_100.value)
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_404_100.value), 404
