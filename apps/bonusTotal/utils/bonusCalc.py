# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     bonusCalc.py
# Description:  奖金计算逻辑
# Author:       'zhouhanlin'
# CreateDate:   2020/02/21
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import json
import pandas as pd
from numpy import NaN
from treelib import Tree
from copy import deepcopy
from datetime import datetime
from flask import current_app
from pandas.io.sql import execute

from apps.common.utils.extensions import scheduler, db
from apps.bonusTotal.utils.pandasExtend import datetime2string
from apps.common.utils.datetimeFormatter import get_month_range

# 设置DataFrame打印属性
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def bonus_audit():
    try:
        with scheduler.app.app_context():
            current_app.logger.info("开始执行定时任务：< bonus_audit >, 计算奖金...")
            sql_1 = 'SELECT room_id, distributor_id, shopkeeper_name, ' + \
                    'create_time FROM t_sales_room order by create_time asc;'
            sql_2 = 'SELECT room_id, distributor_id, distributor_name, introducer_id, superior_id, superior_name, ' + \
                    'order_status, full_or_half_bill, create_time FROM t_bill_info order by create_time asc;'
            # 只有正常状态的经销商参与奖金计算，未审核，已冻结，已取消的经销商不参与奖金计算
            sql_3 = "select distributor_id, create_time from " + \
                    "t_user_info where distributor_status = '正常' " + \
                    "and distributor_id != 'admin' order by create_time asc;"
            df_sales_room = pd.read_sql(sql_1, con=db.engine)
            df_bill = pd.read_sql(sql_2, con=db.engine)
            df_user_info = pd.read_sql(sql_3, con=db.engine)
            df_bonus_summary = bonus_calc(df_sales_room, df_bill, df_user_info)
            # print(df_bonus_summary)
            """
            to_sql的几个参数：
                name是表名
                con是连接
                if_exists：表如果存在怎么处理
                            append：追加
                            replace：删除原表，建立新表再添加
                            fail：什么都不干
                index=False：不插入索引index
            """
            table_name = "t_bonus_summary"
            # print(df_bonus_summary.shape)
            # 先清表，再插入数据
            sql = "truncate table " + table_name + ";"
            execute(sql, db.engine)
            df_bonus_summary.to_sql(name=table_name, con=db.engine, if_exists='append', index=False)
            current_app.logger.info("定时任务：< bonus_audit >执行结束...")
    except Exception as e:
        current_app.logger.error("定时任务：< bonus_audit >执行失败，原因: {}".format(e))


def bonus_calc(df_shop_dis, df_gen_dis, df_user_info):
    """
    奖金计算
    :param DataFrame df_shop_dis: 店铺经销商信息
    :param DataFrame df_gen_dis: 普通经销商信息
    :param DataFrame df_user_info: 经销商并集信息
    :return: DataFrame对象
    """
    """
    Pandas所支持的数据类型: 
    1. float 
    2. int 
    3. bool 
    4. datetime64[ns] 
    5. datetime64[ns, tz] 
    6. timedelta[ns] 
    7. category 
    8. object 
    默认的数据类型是int64,float64.
    """
    type_dict = {
        "phase": "int64",
        "introduce_of_monthly": "int64",
        "introduce_of_total": "int64",
        "order_of_salesroom_monthly": "int64",
        "order_of_salesroom_total": "int64",
        "order_of_monthly": "int64",
        "order_of_total": "int64"
    }
    from apps.common.utils.parse_yaml import ProjectConfig
    config = ProjectConfig.get_object()
    bonus_coefficient = getattr(config, "bonus_coefficient")
    introduce_coefficient = bonus_coefficient.introduce_coefficient
    order_coefficient = bonus_coefficient.order_coefficient
    advanced_bonus_coefficient = bonus_coefficient.advanced_bonus_coefficient

    df_1 = datetime2string(df_shop_dis, "create_time", "total_month", "str", "%Y-%m")
    df_2 = datetime2string(df_gen_dis, "create_time", "total_month", "str", "%Y-%m")
    all_distributor_create_time_list = get_all_distributor_create_time(df_1, df_2, df_user_info)
    df_3 = count_monthly_order(all_distributor_create_time_list, df_1, df_2)
    df_3['allocate_bonus'] = NaN
    df_3 = df_3.astype(dtype=type_dict)
    # 开始计算奖金
    # 1. 介绍奖金
    df_3['introduce_bonus'] = df_3['introduce_of_monthly'].apply(lambda x: x * introduce_coefficient)
    # 2. 营业店铺奖金
    df_3['shop_commission'] = df_3['order_of_salesroom_monthly'].apply(lambda x: x * order_coefficient)
    # 3. 阶段奖金
    df_3['phase_bonus'] = df_3['phase'].apply(lambda x: 10 + advanced_bonus_coefficient * x if x > 0 else 0)
    # 总计奖金, 如果含半单，奖金将减半
    df_3['total_bonus'] = df_3.apply(
        lambda x: x['introduce_bonus'] + x['shop_commission'] + x['phase_bonus'] if x['mark'] is None else
        (x['introduce_bonus'] + x['shop_commission'] + x['phase_bonus']) / 2, axis=1)
    # print(df_3.dtypes)
    return df_3


def tree2dict(tree_obj, nid=None, key=None, sort=True, reverse=False, with_data=False):
    """
    将组织树转成字典
    :param object tree_obj: Tree对象
    :param int nid: 节点标识id(identifier)
    :param int key: 主要是用来进行比较的元素，只有一个参数，具体的函数的参数就是取自于可迭代对象中，指定可迭代对象中的一个元素来进行排序
    :param bool sort: 列表进行排序
    :param bool reverse: 排序规则, True降序, False升序（默认）
    :param bool with_data: 是否涉及data属性数据
    :return: dict
    """

    # 取出根节点
    nid = tree_obj.root if (nid is None) else nid

    # 取出节点标记
    n_tag = tree_obj[nid].tag

    # 拼装字典
    tree_dict = dict()
    tree_dict["id"] = nid
    tree_dict["label"] = n_tag
    tree_dict["children"] = list()

    # 判断是的要取出data属性数据
    if with_data:
        tree_dict["data"] = tree_obj[nid].data

    # 判断是否有扩展
    if tree_obj[nid].expanded:
        # 取出当前节点的下级节点所有属性数据
        queue = [tree_obj[i] for i in tree_obj[nid].fpointer]

        key = (lambda x: x) if (key is None) else key
        # 是否要进行排序
        if sort:
            queue.sort(key=key, reverse=reverse)

        # 遍历下级节点所有属性列表
        for elem in queue:
            tree_dict["children"].append(
                tree2dict(tree_obj, nid=elem.identifier, with_data=with_data, sort=sort, reverse=reverse))
    return tree_dict


def tree2json(tree_obj, with_data=False, sort=True, reverse=False):
    """
    将Tree对象格式化成json字符串.
    :param Tree tree_obj: Tree对象
    :param bool with_data: tree对象中的属性数据
    :param bool sort: 列表进行排序
    :param bool reverse: 排序规则, True降序, False升序（默认）
    :return: json字符串
    """
    return json.dumps(tree2dict(tree_obj, with_data=with_data, sort=sort, reverse=reverse))


def get_all_distributor_create_time(df_object_1, df_object_2, df_user_info):
    """
    获取当前系统中所有的经销商id，name，首次创建时间
    :param DataFrame df_object_1: 门店经销商DataFrame对象
    :param DataFrame df_object_2: 普通经销商DataFrame对象
    :param DataFrame df_user_info: 经销商并集信息
    :return: list
    """
    # 获取门店经销商id，订单经销商id
    df_sales_distributor = df_object_1[['distributor_id',
                                        'shopkeeper_name',
                                        'room_id']].drop_duplicates(subset=["distributor_id"],
                                                                    keep='first',
                                                                    inplace=False)
    df_sales_distributor.rename(columns={"shopkeeper_name": "distributor_name"}, inplace=True)
    df_sales_distributor['full_or_half_bill'] = None
    df_gen_distributor = df_object_2[['distributor_id', 'distributor_name', 'room_id', 'full_or_half_bill']].copy()

    # 将订单信息表与门店信息表数据合并
    df_user_info_1 = df_sales_distributor.append(df_gen_distributor, ignore_index=True)
    # 再将经销商信息表继续关联
    df_user_info_2 = pd.merge(df_user_info, df_user_info_1, how='left', on='distributor_id')
    user_info_lists = df_user_info_2.to_dict('records')
    return user_info_lists


def recursion_find_sub_relation(df_gen_distributor, queue, df_sub_bill):
    """
    递归方法找某个根经销商的所有下级组织关系，不管是横向下级，还是纵向下级
    :param DataFrame df_gen_distributor: DataFrame数据对象
    :param list queue: 存放distributor id和distributor name的列表
    :param DataFrame df_sub_bill: 存放distributor id下级所有子单
    :return: DataFrame
    """
    temp_list = deepcopy(queue)
    if len(queue) == 0:
        return df_sub_bill
    for index, distributor_id in enumerate(temp_list):
        df_sub = df_gen_distributor[df_gen_distributor['superior_id'].isin([distributor_id])]
        queue.remove(distributor_id)
        # 取出当前经销商的直接下级所有经销商
        if not df_sub.empty:
            df_sub_bill = df_sub_bill.append(df_sub, ignore_index=True)
            gen_distributor_lists = df_sub.to_dict('records')
            for gen_distributor_dict in gen_distributor_lists:
                gen_distributor_id = gen_distributor_dict.get("distributor_id")
                queue.append(gen_distributor_id)
        else:
            return df_sub_bill
    return recursion_find_sub_relation(df_gen_distributor, queue, df_sub_bill)


def count_monthly_order(all_distributor_list, df_object_1, df_object_2):
    """
    按每月计算所有订单
    :param list all_distributor_list: 存放所有经销商id的列表
    :param DataFrame df_object_1: 门店经销商DataFrame对象
    :param DataFrame df_object_2: 普通经销商DataFrame对象
    :return: DataFrame
    """
    if len(all_distributor_list) != 0:
        columns = ['distributor_id', 'distributor_name', 'room_id', 'introduce_of_monthly', 'introduce_of_total',
                   'order_of_salesroom_monthly', 'order_of_salesroom_total', 'phase', 'order_of_monthly',
                   'order_of_total', 'mark', 'sub_relation', 'total_month', 'date_joined']
        df_result = pd.DataFrame(columns=columns)
        # 1. 遍历所有经销商
        for distributor_info_dict in all_distributor_list:
            distributor_id = distributor_info_dict.get("distributor_id")
            distributor_name = distributor_info_dict.get("distributor_name")
            room_id = distributor_info_dict.get("room_id")
            full_or_half_bill = distributor_info_dict.get("full_or_half_bill")
            if full_or_half_bill == "否":
                full_or_half_bill = "本人含有半单"
            else:
                full_or_half_bill = None
            create_time = distributor_info_dict.get("create_time")
            # 订单最早产生月份到当前月份之间的所有月份形成的列表
            all_monthly_list = get_month_range(create_time, datetime.now())
            # 存放订单最早产生的月份累计到某一计算的月份
            completed_monthly_list = list()

            # 累计介绍人数
            introduce_of_total = 0

            # 累计所有店铺录入订单数
            order_of_salesroom_total = 0

            # 累计订单数
            order_of_total = 0

            # 组织关系数
            tree = Tree()
            # 创建已自己为根的组织树
            tree.create_node(tag=distributor_name, identifier=distributor_id)
            # 发展下线连续每月订单的统计
            list_1 = list()
            for monthly in all_monthly_list:
                # 当月介绍人数
                introduce_of_monthly = 0
                # 当月所有店铺录入订单数
                order_of_salesroom_monthly = 0
                # 当月订单数
                order_of_monthly = 0
                # 当月是否含半单
                # full_or_half_bill = NaN

                tree_json = tree2json(tree)

                # 2. 按月计算某一经销商的介绍人数
                # 2.1 获取当月介绍人数的订单
                df_introduce_of_monthly = df_object_2[df_object_2['introducer_id'].isin(
                    [distributor_id]) & df_object_2['total_month'].isin([monthly])]
                # 2.2 如果当月有介绍新人订单
                if not df_introduce_of_monthly.empty:
                    introduce_of_monthly = df_introduce_of_monthly.shape[0]
                    introduce_of_total = introduce_of_total + introduce_of_monthly

                # 3. 按月计算某一经销商的所有门店录入的订单数
                # 3.1 获取经销商的所有门店
                df_distributor_all_salesroom = df_object_1[df_object_1['distributor_id'].isin([distributor_id])]
                distributor_all_salesroom_lists = df_distributor_all_salesroom['room_id'].tolist()
                df_order_of_salesroom_monthly = df_object_2[
                    df_object_2['room_id'].isin(distributor_all_salesroom_lists) & df_object_2['total_month'].isin(
                        [monthly])]
                # 3.2 如果当月有新增门店入单
                if not df_order_of_salesroom_monthly.empty:
                    order_of_salesroom_monthly = df_order_of_salesroom_monthly.shape[0]
                    order_of_salesroom_total = order_of_salesroom_total + order_of_salesroom_monthly

                # 4. 按月计算某一经销商的累计的订单数，下线组织关系, 当月订单数, 是否有半单
                # 4.1 获取到某月截止，系统中的所有订单
                completed_monthly_list.append(monthly)
                df_monthly_all_order = df_object_2[df_object_2['total_month'].isin(completed_monthly_list)]
                columns_temp = ['distributor_id', 'distributor_name',
                                'superior_id', 'superior_name',
                                'full_or_half_bill', "total_month"]
                # 4.2 如果某月系统中有新产生的订单
                if not df_monthly_all_order.empty:
                    # 新建一个df存放单个distributor下级的所有订单
                    df_sub_bill = pd.DataFrame(columns=columns_temp)
                    # 4.3 通过递归的方法，获取到某月截止, 某经销所有的下级节点
                    df_sub_bill_temp_1 = recursion_find_sub_relation(df_monthly_all_order, [distributor_id],
                                                                     df_sub_bill)
                    if not df_sub_bill_temp_1.empty:
                        # 4.4 获取某经销商当月所有的下级节点
                        df_sub_bill_temp_2 = df_sub_bill_temp_1[df_sub_bill_temp_1['total_month'].isin([monthly])]
                        if not df_sub_bill_temp_2.empty:
                            order_of_monthly = df_sub_bill_temp_2.shape[0]
                            distributor_month_dict = df_sub_bill_temp_2.to_dict('records')
                            for distributor_dict in distributor_month_dict:
                                distributor_id_temp = distributor_dict.get('distributor_id')
                                distributor_name_temp = distributor_dict.get('distributor_name')
                                superior_id_temp = distributor_dict.get('superior_id')
                                tree.create_node(tag=distributor_name_temp, identifier=distributor_id_temp,
                                                 parent=superior_id_temp)
                            tree_json = tree2json(tree)
                            order_of_total = len(distributor_month_dict)
                list_1.append(order_of_monthly)
                # 5.按月算每个经销商的解阶级数
                # 5.1 某经销商第一次计算阶级
                if len(list_1) == 1:
                    phase = get_phase_number(list_1[0])
                # 5.2 某经销商第二次计算阶级
                elif len(list_1) == 2:
                    list_1_monthly = list_1[0] + list_1[1]
                    phase = get_phase_number(list_1_monthly)
                # 5.3 某经销商第三次计算阶级
                elif len(list_1) == 3:
                    list_1_monthly = list_1[0] + list_1[1] + list_1[2]
                    phase = get_phase_number(list_1_monthly)
                # 5.4 某经销商第四次计算阶级开始一直往后
                else:
                    # 如果出现连续三个月没有下线订单， 清空list_1，重新计算进阶
                    if list_1[-1] == 0 and list_1[-2] == 0 and list_1[-3] == 0:
                        phase = 0
                        list_1.clear()
                    else:
                        total = 0
                        for ele in range(0, len(list_1)):
                            total = total + list_1[ele]
                        phase = get_phase_number(total)
                # 5. 将所有计算的数据拼接到DataFrame中
                data = [distributor_id, distributor_name, room_id, introduce_of_monthly, introduce_of_total,
                        order_of_salesroom_monthly, order_of_salesroom_total, phase, order_of_monthly, order_of_total,
                        full_or_half_bill, tree_json, monthly, create_time]
                df_data = pd.DataFrame(data=[data], columns=columns)
                df_result = df_result.append(df_data, ignore_index=True)
        return df_result


def get_phase_number(bill_total):
    """
    根据订单计算当前阶段数
    :param int bill_total: 订单统计数
    :return: int
    """
    phase = 0
    if bill_total == 0:
        return phase
    while True:
        if 2 ** phase > bill_total:
            return phase - 1
        else:
            phase = phase + 1


def allocate_bonus_calc(month, shop_no, allocate_all_bonus, current_page, per_page):
    """
    计算播出比例奖金
    :param str month: 要计算的月份
    :param str shop_no: 要统计的门店
    :param str allocate_all_bonus: 当月总计要播出的奖金
    :param int current_page: 分页时当前查询页数
    :param int per_page: 每页显示的条目数
    :return: dict
    """
    split_page = {"code": 100,
                  "data": list(),
                  "pages": 1,
                  "total": 0
                  }
    if shop_no:
        sql_1 = "select distributor_id, distributor_name, room_id, phase, introduce_bonus, phase_bonus, " + \
                "shop_commission, total_bonus, mark, total_month from t_bonus_summary " + \
                "where total_month='{} and room_id='{}' order by distributor_id desc ;".format(month, shop_no)
    else:
        sql_1 = "select distributor_id, distributor_name, room_id, phase, introduce_bonus, phase_bonus, " + \
                "shop_commission, total_bonus, mark, total_month from t_bonus_summary " + \
                "where total_month='{}' order by distributor_id desc ;".format(month)
    df_distributor_bill = pd.read_sql(sql_1, con=db.engine)
    if df_distributor_bill.empty:
        split_page["code"] = 100
        split_page["status"] = 404
        return split_page
    # 当月预期总奖金
    expected_total_bonus = df_distributor_bill['total_bonus'].sum()
    # 当月总订单条目数
    total_bill = df_distributor_bill.shape[0]
    if total_bill % per_page == 0:
        split_page["pages"] = total_bill // per_page
    else:
        split_page["pages"] = total_bill // per_page + 1
    if current_page > split_page["pages"]:
        split_page["code"] = 100
        split_page["status"] = 400
        split_page.pop('pages')
        return split_page

    offset_start = (current_page - 1) * per_page
    offset_end = current_page * per_page
    # DataFrame数据按index取offset_start到offset_end区间的数据
    df_distributor_bill = df_distributor_bill.iloc[offset_start:offset_end]

    # 计算每个经销商的播出比例奖金，保留小数点后三位
    df_distributor_bill['allocate_bonus'] = df_distributor_bill['total_bonus'].apply(
        lambda x: round((x / expected_total_bonus) * eval(allocate_all_bonus), 3))
    df_distributor_bill['allocate_bonus'].fillna(0.000, inplace=True)
    # 将DataFrame转化成列表
    distributor_bill_lists = df_distributor_bill.to_dict('records')
    split_page['data'] = distributor_bill_lists
    split_page['current_page'] = current_page
    split_page['per_page'] = per_page
    split_page['expected_total_bonus'] = expected_total_bonus
    split_page['total'] = total_bill
    return split_page


if __name__ == "__main__":
    import pymysql

    conn = pymysql.connect(host="172.16.8.91",
                           user="zenops",
                           passwd="zenops",
                           db="zen",
                           port=3306,
                           charset="utf8"
                           )

    sql_4 = 'SELECT room_id, distributor_id, shopkeeper_name, ' + \
            'create_time FROM t_sales_room order by create_time asc;'
    sql_5 = 'SELECT room_id, distributor_id, distributor_name, introducer_id, superior_id, superior_name, ' + \
            'order_status, full_or_half_bill, create_time FROM t_bill_info order by create_time asc;'
    # 只有正常状态的经销商参与奖金计算，未审核，已冻结，已取消的经销商不参与奖金计算
    sql_6 = "select distributor_id, create_time from " + \
            "t_user_info where distributor_status = '正常' " + \
            "and distributor_id != 'admin' order by create_time asc;"
    df_sales_room_temp = pd.read_sql(sql_4, con=conn)
    df_bill_temp = pd.read_sql(sql_5, con=conn)
    df_user_info_temp = pd.read_sql(sql_6, con=conn)
    df_8 = bonus_calc(df_sales_room_temp, df_bill_temp, df_user_info_temp)
    print(df_8)
    """
    to_sql的几个参数：
        name是表名
        con是连接
        if_exists：表如果存在怎么处理
                    append：追加
                    replace：删除原表，建立新表再添加
                    fail：什么都不干
        index=False：不插入索引index
    """
    # df_8.to_sql(name='t_bonus_summary', con=conn, if_exists='replace', index=False)
    conn.close()  # 使用完后记得关掉
