# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     bonusSummary.py
# Description:  奖金报表数据模型
# Author:       'zhouhanlin'
# CreateDate:   2020/02/21
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from sqlalchemy import Column, String, Integer, DECIMAL, BLOB, DateTime
from apps.common.utils.base_model import BaseModel


class BonusSummary(BaseModel):
    """
    专卖店数据模型
    SQL建表语句：
                drop table if exists `t_bonus_summary`;
                create table `t_bonus_summary` (
                `id` int ( 11 ) not null auto_increment comment '主键',
                `distributor_id` varchar ( 255 ) not null comment '经销商id,
                `distributor_name` varchar ( 255 ) not null comment '经销商名称',
                `room_id` varchar ( 255 ) not null comment '店铺id',
                `phase` int ( 4 ) not null comment '阶段',
                `sub_relation` longblob not null comment '下级关系',
                `introduce_bonus` decimal (6, 2) not null comment '介绍奖金',
                `phase_bonus` decimal (6, 2) not null comment '阶段奖金',
                `shop_commission` decimal (6, 2) not null comment '店铺佣金',
                `total_bonus` decimal (12, 2) not null comment '总计奖金',
                `allocate_bonus` decimal (12, 2) null comment '播出比例奖金',
                `mark` varchar ( 255 ) null comment '备注',
                `total_month` varchar ( 10 ) not null comment '统计月份'
                primary key ( `id` )
                ) default charset = utf8 comment '奖金统计信息表';
    """
    __tablename__ = "t_bonus_summary"
    id = Column("id", Integer, primary_key=True, autoincrement=True, comment="主键")
    # key = Column("key", String(255), nullable=False, unique=True, comment="经销商id+总计奖金+统计月份组合索引，值唯一")
    distributor_id = Column("distributor_id", String(64), nullable=False, comment="经销商id，值唯一")
    distributor_name = Column("distributor_name", String(255), nullable=False, comment="经销商名称")
    introduce_of_monthly = Column("introduce_of_monthly", Integer, nullable=False, comment="当月介绍新人数")
    introduce_of_total = Column("introduce_of_total", Integer, nullable=False, comment="截止当月累计介绍新人数")
    order_of_salesroom_monthly = Column("order_of_salesroom_monthly", Integer, nullable=False, comment="经销商门店当月录单总数")
    order_of_salesroom_total = Column("order_of_salesroom_total", Integer, nullable=False, comment="截止当月经销商门店累计录单总数")
    phase = Column("phase", Integer, nullable=False, comment="阶段")
    order_of_monthly = Column("order_of_monthly", Integer, nullable=False, comment="当月新增下线订单数")
    order_of_total = Column("order_of_total", Integer, nullable=False, comment="截止当月累计新增下线订单数")
    sub_relation = Column("sub_relation", BLOB, nullable=False, comment="下级关系")
    introduce_bonus = Column("introduce_bonus", Integer, nullable=False, comment="介绍奖金")
    room_id = Column("room_id", String(255), nullable=False, comment="门店id")
    phase_bonus = Column("phase_bonus", Integer, nullable=False, comment="阶段奖金")
    shop_commission = Column("shop_commission", DECIMAL(6, 2), nullable=False, comment="店铺佣金")
    total_bonus = Column("total_bonus", DECIMAL(12, 2), nullable=False, comment="总计奖金")
    allocate_bonus = Column("allocate_bonus", DECIMAL(12, 2), nullable=True, comment="播出比例奖金")
    mark = Column("mark", String(255), nullable=True, comment="备注")
    total_month = Column("total_month", String(10), nullable=False, comment="统计月份")
    date_joined = Column("date_joined", DateTime, nullable=True, comment="经销商加入时间")

    # 给表添加注释
    __table_args__ = ({'comment': '奖金统计信息表'})
