# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     noticeInfo.py
# Description:  通知消息数据模型
# Author:       'zhouhanlin'
# CreateDate:   2020/03/01
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from sqlalchemy import Column, String, Integer, Date, Text

from apps.common.utils.base_model import BaseModel


class NoticeInfo(BaseModel):
    """
    通知信息表模型
    SQL建表语句：
                drop table if exists `t_notice_info`;
                create table `t_notice_info` (
                `id` int ( 11 ) not null auto_increment comment '主键',
                `notice_title` varchar ( 255 ) not null comment '通知标题',
                `start_time` date not null comment '通知开始生效时间',
                `end_time` date not null comment '通知开始失效时间',
                `status` varchar ( 32 ) not null default '待编辑' comment '通知状态',
                `create_time` datetime DEFAULT CURRENT_TIMESTAMP comment '通知创建时间',
                `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '通知信息修改时间',
                primary key ( `id` )
                ) default charset = utf8 comment '通知信息表';
    """
    __tablename__ = "t_notice_info"
    id = Column("id", Integer, primary_key=True, autoincrement=True, comment="主键")
    notice_title = Column("notice_title", String(255), nullable=False, comment="通知标题")
    start_time = Column("start_time", Date, nullable=False, comment="通知开始生效时间")
    end_time = Column("end_time", Date, nullable=False, comment="通知开始失效时间")
    status = Column("status", String(32), nullable=False, server_default='待编辑', comment="通知状态")
    notice_content = Column("notice_content", String(1000), nullable=True, comment="通知内容")
    read_user_id = Column("read_user_id", Text, nullable=True, comment="已读用户")

    # 给表添加注释
    __table_args__ = ({'comment': '通知信息表'})
