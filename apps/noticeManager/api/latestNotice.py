# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     latestNotice.py
# Description:  最新通知
# Author:       'zhouhanlin'
# CreateDate:   2020/03/15
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from datetime import date
from flask_restful import fields, reqparse, marshal

from apps.common.utils.base_resource import BaseResource
from apps.common.http.decorators import login_required
from apps.common.http.respBody import RespBody, MsgDesc
from apps.noticeManager.models.noticeInfo import NoticeInfo

query_fields = {
    'id': fields.Integer,
    'notice_title': fields.String,
    'notice_content': fields.String,
    'create_time': fields.String
}


class LatestNoticeApi(BaseResource):

    @login_required
    def get(self):
        """
        通知实时推送接口
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 降序, 必须要在生效时间内
        current_date = date.today()
        data = NoticeInfo.query.filter(
            NoticeInfo.start_time <= current_date,
            NoticeInfo.end_time >= current_date,
            NoticeInfo.status == "已推送").order_by(NoticeInfo.create_time.desc()).first()
        self.logger.info(self.logger_formatter + " 成功...")
        return RespBody.custom(result="success", code=200, string=MsgDesc.h_200_200.value,
                               data=marshal(data, query_fields)), 200
        # if data is not None:
        #     self.logger.info(self.logger_formatter + " 成功...")
        #     return RespBody.custom(result="success", code=200, string=MsgDesc.h_200_200.value,
        #                            data=marshal(data, query_fields)), 200
        # else:
        #     self.logger.error(MsgDesc.h_404_100.value)
        #     self.logger.error(self.logger_formatter + " 失败...")
        #     return RespBody.custom(string=MsgDesc.h_404_100.value), 404
