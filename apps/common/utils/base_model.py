# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  envcenter
# FileName:     base_model.py
# Description:  ORM基础模型
# Author:       zhouhanlin
# CreateDate:   2020/10/26
# Copyright ©2011-2020. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import json
from sqlalchemy.sql.sqltypes import JSON
from sqlalchemy import TIMESTAMP, text, Column, inspect

from apps.common.utils.extensions import db

__all__ = ['BaseModel']


class AdminMixin(object):
    """
    数据管理模型
    """
    create_time = Column("create_time",
                         TIMESTAMP(True),
                         server_default=text('CURRENT_TIMESTAMP'),
                         nullable=False,
                         comment="单据创建时间'"
                         )
    # 条目更新的时候，时间自动更新
    update_time = Column("update_time",
                         TIMESTAMP(True),
                         server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
                         nullable=False,
                         comment="单据信息修改时间"
                         )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class BaseModel(db.Model, AdminMixin):
    # __abstract__这个属性设置为True,这个类为基类，不会被创建为表！
    __abstract__ = True

    @staticmethod
    def str_is_object(args: str) -> bool:
        try:
            value = json.loads(args)
            if isinstance(value, list) or isinstance(value, dict):
                return True
            else:
                return False
        except (json.decoder.JSONDecodeError, TypeError):
            return False

    def to_dict(self):
        """
        转换为字典datetime为字符串，json自动解析
        """
        d = {}
        for column_name, column_attr in self.columns_attr_dict().items():
            if isinstance(column_attr.get("type"), JSON) and column_attr.get("value") is not None:
                if self.str_is_object(column_attr.get("value")) or \
                        isinstance(column_attr.get("value"), bytes) or \
                        isinstance(column_attr.get("value"), bytearray):
                    v = json.loads(column_attr.get("value"))
                else:
                    v = column_attr.get("value")
            else:
                v = column_attr.get("value")
            d[column_name] = v
        return d

    def update(self, **kwargs):
        columns_dict = self.columns_attr_dict()
        for key, value in kwargs.items():
            # 过滤掉参数值为空和主键
            if key in columns_dict.keys() and not columns_dict.get(key).get("primary_key") and value is not None:
                setattr(self, key, value)

    def columns_attr_dict(self):
        # from sqlalchemy.sql.schema import Column
        columns_attr_dict = inspect(self).mapper.column_attrs
        # 获取模型列的必填属性，必填为True，非必填为False
        data = dict()
        for column_name, column_attr in columns_attr_dict.items():
            column_attr_dict = dict(value=getattr(self, column_attr.key),
                                    nullable=column_attr.columns[0].nullable,
                                    primary_key=column_attr.columns[0].primary_key,
                                    default=column_attr.columns[0].default,
                                    server_default=column_attr.columns[0].server_default,
                                    server_onupdate=column_attr.columns[0].server_onupdate,
                                    index=column_attr.columns[0].index,
                                    unique=column_attr.columns[0].unique,
                                    doc=column_attr.columns[0].doc,
                                    onupdate=column_attr.columns[0].onupdate,
                                    autoincrement=column_attr.columns[0].autoincrement,
                                    constraints=column_attr.columns[0].constraints,
                                    foreign_keys=column_attr.columns[0].foreign_keys,
                                    comment=column_attr.columns[0].comment,
                                    type=column_attr.columns[0].type,
                                    computed=column_attr.columns[0].computed,
                                    )
            data[column_name] = column_attr_dict
        return data
