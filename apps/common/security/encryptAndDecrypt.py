# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     encryptAndDecrypt.py
# Description:  python常用的加密解密
# Author:       'zhouhanlin'
# CreateDate:   2020/02/28
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import rsa
import base64
from binascii import b2a_hex, a2b_hex

# windows包：Cryptodome
# linux包： Crypto
try:
    from Cryptodome import Random
    from Cryptodome.Cipher import AES
except ImportError:
    from Crypto import Random
    from Crypto.Cipher import AES


def str_to_byte(string, encoding='utf-8', errors='strict'):
    """
    字符转字节
    :param str string: 要转化的字符或字符串
    :param str encoding: 编码集，默认是utf-8
    :param str errors: 转码时对错误的处理方式 ['strict','ignore','replace','xmlcharrefreplace', ...]
                       默认是 strict
    :return:
    """
    if isinstance(string, str):
        b = string.encode(encoding=encoding, errors=errors)
        return b
    else:
        raise Exception("参数类型错误.")


def byte_to_str(byte_obj, encoding='utf-8', errors='strict'):
    """
    字节转字符
    :param Byte byte_obj: 字节对象
    :param str encoding: 解码集，默认是utf-8
    :param str errors: 转码时对错误的处理方式 ['strict','ignore','replace','xmlcharrefreplace', ...]
                       默认是 strict
    :return: str
    """
    if isinstance(byte_obj, bytes):
        string = byte_obj.decode(encoding=encoding, errors=errors)
        return string
    else:
        raise Exception("参数类型错误.")


def str_serializable(string, encoding='utf-8', errors='strict'):
    """
    字符串序列化字节
    :param str string: 需要序列化的字符或者字符
    :param str encoding: 编码集，默认是utf-8
    :param str errors: 转码时对错误的处理方式 ['strict','ignore','replace','xmlcharrefreplace', ...]
                       默认是 strict
    :return: 序列化字节 如：b'e58d97e58c97'
    """
    if isinstance(string, str):
        b2a = b2a_hex(string.encode(encoding=encoding, errors=errors)).decode(encoding=encoding, errors=errors)
        return b2a
    else:
        raise Exception("参数类型错误.")


def str_deserializable(serialization_obj, encoding='utf-8', errors='strict'):
    """
    字符串反序列化字节
    :param byte serialization_obj: 序列化对象，如：b'e58d97e58c97'
    :param str encoding: 解码集，默认是utf-8
    :param str errors: 转码时对错误的处理方式 ['strict','ignore','replace','xmlcharrefreplace', ...]
                       默认是 strict
    :return: str
    """
    if isinstance(serialization_obj, str):
        string = a2b_hex(serialization_obj.encode(encoding=encoding,
                                                  errors=errors)).decode(encoding=encoding, errors=errors)
        return string
    else:
        raise Exception("参数类型错误.")


def safe_str_cmp(new_str, old_str):
    """
    安全字符串比较
    :param str new_str: 新字符串
    :param str old_str: 老字符串
    :return: bool
    """
    if isinstance(new_str, str) and isinstance(old_str, str):
        temp_str = str_serializable(new_str)
        if temp_str == old_str:
            return True
        else:
            return False
    else:
        raise Exception("参数类型错误.")


class RsaCrypt(object):
    def __init__(self, crypt_length=256):
        self.pub_key, self.pri_key = rsa.newkeys(crypt_length)

    def encrypt(self, text):
        cipher_text = rsa.encrypt(text.encode(), self.pub_key)
        # 因为rsa加密时候得到的字符串不一定是ascii字符集的，
        # 输出到终端或者保存时候可能存在问题，所以这里统一把加密后的字符串转化为16进制字符串
        return b2a_hex(cipher_text).decode()

    def decrypt(self, cipher_text):
        decrypt_text = rsa.decrypt(a2b_hex(cipher_text.encode()), self.pri_key)
        return decrypt_text.decode()


class AesCrypt(object):

    def __init__(self, model='ECB', encode='utf-8'):
        """
        构造函数
        :param model:  常用的有 ECB，CBC，CFB等模式
        :param encode:
        """
        self.__encode = encode
        self.__model = {'ECB': AES.MODE_ECB,
                        'CBC': AES.MODE_CBC,
                        'CFB': AES.MODE_CFB}[model]
        # 密钥key 长度必须为16（AES-128）、24（AES-192）、或32（AES-256）Bytes 长度.
        # 目前AES-256
        self.__key = b"this is a 16 key this is a 3 key"
        # 生成长度等于AES块大小的不可重复的密钥向量
        self.__iv = Random.new().read(AES.block_size)
        # iv 偏移量：这个参数在ECB模式下不需要，在CBC，CFB模式下需要
        if model == 'ECB':
            # 创建一个aes对象
            self.my_cipher = AES.new(self.__key, self.__model)
        elif model == 'CBC':
            # 创建一个aes对象
            self.my_cipher = AES.new(self.__key, self.__model, self.__iv)
        elif model == 'CFB':
            # 创建一个aes对象
            self.my_cipher = AES.new(self.__key, self.__model, self.__iv)

    # 如果text不足16位的倍数就用空格补足为16位
    def __add_16(self, par):
        par = par.encode(self.__encode)
        while len(par) % 16 != 0:
            par += b'\x00'
        return par

    def encrypt(self, text):
        # 加密的明文长度必须为16的倍数，如果长度不为16的倍数，则需要补足为16的倍数
        # 将iv（密钥向量）加到加密的密文开头，一起传输
        text = self.__add_16(text)
        cipher_text = self.my_cipher.encrypt(text)
        # 因为AES加密后的字符串不一定是ascii字符集的，输出保存可能存在问题，所以这里转为16进制字符串
        return base64.encodebytes(cipher_text).decode().strip()

    def decrypt(self, cipher_text):
        # 解密的话要用key和iv生成新的AES对象
        text = base64.decodebytes(cipher_text.encode(self.__encode))
        decrypt_text = self.my_cipher.decrypt(text)
        return decrypt_text.decode(self.__encode).strip('\0')


rsa_crypt = RsaCrypt()
aes_crypt = AesCrypt()

if __name__ == "__main__":
    # a = str_serializable("119002")
    # print(a)

    # b = str_deserializable(a)
    # print(b)
    # te = 'admin'
    # ency_text = rsa_crypt.encrypt(te)
    # print(rsa_crypt.decrypt(ency_text))
    d = "000205"
    aes = AesCrypt()
    c = aes_crypt.encrypt(d)
    print(c)
    # c = "6f8c411c1130a18dbfc385e632a458c60a9c236220dbb7e4dd5f8484e336b87f"
    # c = "f4047e14292f5039b4106aa04d595a7f26fb75f2156f88ac3fa03ca0cf1d00cb"
    de = aes.decrypt(c)
    print([de])
