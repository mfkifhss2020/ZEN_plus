# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     bluePrint.py
# Description:  蓝本入口
# Author:       zhouhanlin
# CreateDate:   2020/04/07
# Copyright ©2011-2020. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from flask import Blueprint

from apps.common.http.restfulApi import RestfulApi
from apps.userManager.api.userInfoApi import UserInfo
from apps.userManager.api.userSubOrgApi import UserSubOrg
from apps.userManager.api.loginCheckApi import LoginCheck
from apps.userManager.api.logoutCheckApi import LogoutCheck
from apps.billManager.api.billManagerApi import BillInfoApi
from apps.salesRoomManager.sales_views import ShopNoListView
from apps.userManager.api.updatePasswdApi import UpdatePassword
from apps.salesRoomManager.api.salesRoomApi import SalesRoomApi
from apps.bonusTotal.api.bonusSummaryApi import BonusSummaryApi
from apps.noticeManager.api.realtimePush import RealtimePushApi
from apps.noticeManager.api.latestNotice import LatestNoticeApi
from apps.noticeManager.api.unreadNotice import UnreadNoticeApi
from apps.billManager.api.distributorNameApi import DistributorName
from apps.noticeManager.api.noticeManagerApi import NoticeManagerApi

blueprint = Blueprint('blueprint', __name__)
api = RestfulApi(blueprint, catch_all_404s=False)

# 添加路由
api.add_resource(SalesRoomApi, '/salesRoomManager/salesRoom', endpoint='salesRoom')
api.add_resource(ShopNoListView, '/salesRoomManager/shopNoList', endpoint='shopNoList')
api.add_resource(BillInfoApi, '/billManager/billInfo', endpoint='billInfo')
api.add_resource(BonusSummaryApi, '/bonusTotal/bonusSummary', endpoint='bonusSummary')
api.add_resource(NoticeManagerApi, '/noticeManager/noticeInfo', endpoint='noticeInfo')
api.add_resource(RealtimePushApi, '/noticeManager/realtimePush', endpoint='realtimePush')
api.add_resource(UserSubOrg, '/userManager/userSubOrg', endpoint='userSubOrg')
api.add_resource(DistributorName, '/billManager/distributorName', endpoint='distributorName')

api.add_resource(UserInfo, '/userManager/userInfo', endpoint='userInfo')
api.add_resource(LoginCheck, '/userManager/login', endpoint='login')
api.add_resource(LogoutCheck, '/userManager/logout', endpoint='logout')
api.add_resource(UpdatePassword, '/userManager/updatePassword', endpoint='updatePassword')
api.add_resource(LatestNoticeApi, '/userManager/latestNotice', endpoint='latestNotice')
api.add_resource(UnreadNoticeApi, '/userManager/unreadNotice', endpoint='unreadNotice')

