# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     loginCheckApi.py
# Description:  用户登录校验Api
# Author:       'zhouhanlin'
# CreateDate:   2020/02/19
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from flask_restful import reqparse
from flask import session

from apps.common.utils.base_resource import BaseResource
from apps.userManager.models.User import User
from apps.billManager.models.billInfo import BillInfo
from apps.common.http.decorators import login_required
from apps.common.utils.paramCheck import is_isalnum_ex
from apps.common.http.respBody import RespBody, MsgDesc
from apps.salesRoomManager.models.salesRoom import SalesRoom


class LoginCheck(BaseResource):

    @login_required
    def get(self):
        """
        获取登录用户基本信息
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 获取登录用户session中的distributor_id
        distributor_id = session.get("distributor_id")
        info_dict = {"distributor_id": distributor_id}
        if distributor_id == "admin":
            info_dict["distributor_name"] = "管理员"
        else:
            record_1 = SalesRoom.query.filter_by(distributor_id=distributor_id).first()
            record_2 = BillInfo.query.filter_by(distributor_id=distributor_id).first()
            if record_1:
                info_dict["distributor_name"] = record_1.shopkeeper_name
            elif record_2:
                info_dict["distributor_name"] = record_2.distributor_name
        self.logger.info(self.logger_formatter + " 成功...")
        return RespBody.custom(result="success", code=200, string=MsgDesc.h_200_200.value, data=[info_dict]), 200

    def post(self):
        """
        用户登录校验
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取args中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("distributor_id", type=str, required=True, location='form')
        parse.add_argument("password", type=str, required=True, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        distributor_id = args.get("distributor_id")
        password = args.get("password")
        if is_isalnum_ex(distributor_id):
            record_1 = User.query.filter_by(distributor_id=distributor_id).first()
            if record_1 and record_1.check_password(password):
                if record_1.distributor_status == '正常':
                    # 认证通过，将经销商id添加到session中
                    session['distributor_id'] = record_1.distributor_id
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success", code=201, string=MsgDesc.h_201_201.value), 201
                else:
                    self.logger.error(MsgDesc.h_401_107.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(code=107, string=MsgDesc.h_401_107.value), 401
            else:
                self.logger.error(MsgDesc.h_401_106.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=106, string=MsgDesc.h_401_106.value), 401
        else:
            self.logger.error(MsgDesc.h_401_124.value)
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=106, string=MsgDesc.h_401_124.value), 401
