# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     logoutCheckApi.py
# Description:  用户退出登录API
# Author:       'zhouhanlin'
# CreateDate:   2020/02/28
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from flask_restful import reqparse
from flask import session

from apps.common.utils.base_resource import BaseResource
from apps.common.http.decorators import login_required
from apps.common.http.respBody import RespBody, MsgDesc


class LogoutCheck(BaseResource):

    # 装饰器判断用户是否已经登录
    @login_required
    def put(self):
        """
        用户退出系统
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 删除session中的distributor_id
        distributor_id = session.pop("distributor_id")
        # del session("distributor_id"）
        # 清除所有distributor_id, 慎用
        # session.clear()

        self.logger.info(MsgDesc.h_201_202.value + "<{}>".format(distributor_id))
        self.logger.info(self.logger_formatter + " 成功...")
        return RespBody.custom(result="success", code=202, string=MsgDesc.h_201_202.value), 201
