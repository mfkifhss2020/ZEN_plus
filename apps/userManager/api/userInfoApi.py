# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     userInfoApi.py
# Description:  用户注册API
# Author:       'zhouhanlin'
# CreateDate:   2020/02/19
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from flask_restful import fields, marshal, reqparse

from apps.common.utils.extensions import db
from apps.common.utils.base_resource import BaseResource
from apps.userManager.models.User import User
from apps.common.http.respBody import RespBody, MsgDesc
from apps.common.security.encryptAndDecrypt import aes_crypt
from apps.userManager.utils.userSupplement import rich_user_info
from apps.common.http.decorators import login_required, access_auth

query_fields = {
    'distributor_id': fields.String,
    'distributor_name': fields.String,
    'certificate_id': fields.String,
    'contact': fields.String,
    'password': fields.String,
    'create_time': fields.String,
    'distributor_status': fields.String
}


class UserInfo(BaseResource):

    @login_required
    @access_auth
    def get(self):
        """
        给前端提供数据查询服务，支持模糊搜索查询
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取args中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("current_page", type=int, help="当前页数校验错误", required=True, location='args')
        parse.add_argument("per_page", type=int,
                           help="必须设置页码条目数为10，20，50，或者100",
                           choices=[10, 20, 50, 100],
                           required=True,
                           location='args'
                           )

        parse.add_argument("keyword", type=str, required=False, location='args')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        keyword = args.get("keyword")
        current_page = args.get("current_page")
        per_page = args.get("per_page")
        result = rich_user_info(current_page, per_page, keyword)
        if result.get('status') == 400:
            self.logger.error(MsgDesc.h_400_100.value)
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_400_100.value), 400
        elif result.get('status') == 404:
            self.logger.error(MsgDesc.h_404_100.value)
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_404_100.value), 404
        elif result.get('status') is None:
            result['data'] = marshal(result.get('data'), query_fields)
            result['code'] = 200
            result['message'] = MsgDesc.h_200_200.value
            self.logger.info(self.logger_formatter + " 成功...")
            return result, 200

    @login_required
    @access_auth
    def put(self):
        """
        完成前端用户冻结，解冻操作
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("distributor_id", type=str, help='用户id验证错误', required=True, location='form')
        parse.add_argument("action",
                           type=str,
                           choices=["冻结", "解冻"],
                           help='只能进行冻结，解冻操作',
                           required=True, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        distributor_id = args.get("distributor_id")
        action = args.get("action")
        record = User.query.filter_by(distributor_id=distributor_id).first()
        if record is None:
            self.logger.error(MsgDesc.h_401_105.value + "<{}>".format(distributor_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=105, string=MsgDesc.h_401_105.value), 401
        else:
            if action == "冻结":
                if record.distributor_status == "正常":
                    record.distributor_status = "已冻结"
                else:
                    self.logger.error(MsgDesc.h_401_114.value + "<{}>".format(distributor_id))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(code=114, string=MsgDesc.h_401_114.value), 401
            elif action == "解冻":
                if record.distributor_status == "已冻结":
                    record.distributor_status = "正常"
                else:
                    self.logger.error(MsgDesc.h_401_113.value + "<{}>".format(distributor_id))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(code=113, string=MsgDesc.h_401_113.value), 401
            else:
                self.logger.error(MsgDesc.h_401_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_401_100.value), 401
        try:
            db.session.commit()
            db.session.remove()
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success", code=200, string=MsgDesc.h_201_200.value), 201
        except Exception as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_411_100.value), 411

    @login_required
    @access_auth
    def post(self):
        """
        完成前端页面密码重置功能
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("distributor_id", type=str, help='用户id验证错误', required=True, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        distributor_id = args.get("distributor_id")
        record = User.query.filter_by(distributor_id=distributor_id).first()
        if record is None:
            self.logger.error(MsgDesc.h_401_105.value + "<{}>".format(distributor_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=105, string=MsgDesc.h_401_105.value), 401
        else:
            record.password = aes_crypt.encrypt(distributor_id[-6:])
        try:
            db.session.commit()
            db.session.remove()
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success", code=205, string=MsgDesc.h_201_205.value), 201
        except Exception as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_411_100.value), 411
