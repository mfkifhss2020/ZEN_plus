# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     updatePasswdApi.py
# Description:  修改用户密码Api
# Author:       'zhouhanlin'
# CreateDate:   2020/02/19
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from flask_restful import reqparse
from flask import session

from apps.common.utils.extensions import db
from apps.common.utils.base_resource import BaseResource
from apps.userManager.models.User import User
from apps.common.http.decorators import login_required
from apps.common.http.respBody import RespBody, MsgDesc
from apps.common.security.encryptAndDecrypt import aes_crypt


class UpdatePassword(BaseResource):

    @login_required
    def put(self):
        """
        用户密码修改
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("original_password", type=str, help='原密码验证错误', required=True, location='form')
        parse.add_argument("new_address", type=str, help='新密码验证错误', required=True, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        original_password = args.get("original_password")
        new_address = args.get("new_address")
        if original_password == new_address:
            self.logger.error(MsgDesc.h_401_109.value)
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=109, string=MsgDesc.h_401_109.value), 401
        if 6 <= len(new_address) <= 16:
            distributor_id = session.get("distributor_id")
            record = User.query.filter_by(distributor_id=distributor_id).first()
            if record.check_password(original_password):
                record.password = aes_crypt.encrypt(new_address)
                try:
                    db.session.commit()
                    db.session.remove()
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success", code=204, string=MsgDesc.h_201_204.value), 201
                except Exception as e:
                    db.session.rollback()
                    db.session.remove()
                    self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_411_100.value), 411
            else:
                self.logger.error(MsgDesc.h_401_110.value + "<{}>".format(original_password))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=110, string=MsgDesc.h_401_110.value), 401
        else:
            self.logger.error(MsgDesc.h_401_111.value + "<{}>".format(new_address))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=111, string=MsgDesc.h_401_111.value), 401
