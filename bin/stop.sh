!/bin/bash
user=$USER
param_1="$1"
if [ $user == "zenops" ];then
    zen_processid_list=`ps -ef|grep /opt/ZEN/flask_app.py|grep -v grep|cut -c 9-15`
	if [ ! -n "$param_1" ];then
		if [ ! -n "$zen_processid_list" ];then
			echo "zen服务没有在运行."
		else
			`ps -ef|grep /opt/ZEN/flask_app.py|grep -v grep|cut -c 9-15 | xargs kill -9`
			echo "zen服务已经停止..."
		fi
		# echo "所有服务停止完成..."
	else
		if [ $param_1 == "zenops" ];then
			if [ ! -n "$zen_processid_list" ];then
				echo -e "zen服务没有在运行.\n"
			else
				`ps -ef|grep /opt/ZEN/flask_app.py|grep -v grep|cut -c 9-15 | xargs kill -9`
				echo -e "zen服务已经停止...\n"
			fi
		else
			echo "输入的参数错误."
			echo -e "例: sh stop.sh [不带参数|zen]\n"
		fi
	fi
else
    echo "警告！请使用zenops用户进行相关操作."
fi	
