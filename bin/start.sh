#!/bin/bash
param_1="$1"
execute="$HOME/ZEN_ENV36/bin/python"
if [ $USER == "zenops" ];then
    zen_processid_list=`ps -ef|grep /opt/ZEN/flask_app.py|grep -v grep|cut -c 9-15`
	`source $HOME/ZEN_ENV36/bin/activate`
	if [ ! -n "$param_1" ];then
		if [ ! -n "$zen_processid_list" ];then
			`nohup $execute /opt/ZEN/flask_app.py >/dev/null 2>&1 &`
			echo -e "zen服务启动成功."
		else
			echo "zen服务已经在运行."
		fi
		# echo -e "所有服务启动完成...\n"
	else
		if [ $param_1 == "zen" ];then
			if [ ! -n "$zen_processid_list" ];then
				`nohup $execute /opt/ZEN/flask_app.py >/dev/null 2>&1 &`
				echo -e "zen服务启动成功.\n"
			else
				echo -e "zen服务已经在运行...\n"
			fi
		else
			echo "输入的参数错误."
			echo -e "例: sh start.sh [不带参数|zen]\n"
		fi
	fi
else
    echo -e "警告！请使用zenops用户进行相关操作.\n" 
fi
