#!/bin/bash
user=$USER
if [ $user=="zenops" ];then
    zen_processid_list=`ps -ef|grep /opt/ZEN/flask_app.py|grep -v grep|cut -c 9-15`
    if [ -n "$zen_processid_list" ];then
        echo "zen服务正在运行."
    else
        echo "zen服务没有运行."
    fi
else
    echo "警告！请使用zenops用户进行相关操作."
fi
