python 运行环境基础镜像
===

摘要
---
* 包含s6进程管理工具
* python3.6.8
* 默认app run 脚本
* 需要代码里面自行实现 start.sh
```
#!/bin/bash
cd /appruntime/java/${APP_NAME}
if [[ -f /appruntime/java/start.sh ]]; then
    echo "/bin/bash /appruntime/java/start.sh"
    /bin/bash /appruntime/java/start.sh || exit 1
elif [[ "${APP_BIN}" != "" ]]; then
    echo "python ${APP_BIN}"
    ${APP_BIN} || exit 1
else
    echo "python ${APP_NAME}"
    python ${APP_NAME} || exit 1    
fi
```

* 构建时需要传入一些参数
```
docker build -f APP-META/docker-config/default/Dockerfile  --build-arg ARCHIVE=. --build-arg APP_NAME=${APP_NAME} --build-arg APP_BIN=${APP_BIN} --build-arg ENV_TYPE=${ENV_TYPE} -t harbor-k8s.xsyxsc.cn/apps/${APP_NAME}:${TIMESTAMP}____${ENV_TYPE} .
```
* ARCHIVE 需要打到镜像的文件或目录。
* APP_NAME 应用名
* APP_BIN 需要运行的文件名
* ENV_TYPE 运行的环境
* 可以直接在 Dockerfile 里面设置默认值
