create database zen charset='utf8';  -- 建库
create user 'zenops'@'%' identified by 'Zenops@123';  -- 创建用户， %是占位符，匹配所有，谨慎使用
grant all privileges on zen.* to 'zenops'@'%' identified by 'Zenops@123';  -- 5.7 版本赋予增删改查权限
grant all privileges on zen.* to 'zenops'@'%'; -- 8.0 版本赋予增删改查权限
alter user 'zen'@'%' identified with mysql_native_password by 'Zenops@123';  -- 8.0 修改用户密码属性
flush privileges;  -- 刷新配置



# 数据插入
insert into `t_user_info` ( `distributor_id`, `password` ) values ( 'admin','61646d696e' );
